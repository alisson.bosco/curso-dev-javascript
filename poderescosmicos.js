/** 
class Pessoa {
    nome = "HUM"
}

class Americano extends Pessoa {

}

console.log(new Americano())
 */


function Pessoa() {
    this.nome = "HUM"
}

function Americano() {
    this.prototype = Pessoa.call(this)
}


// console.log(new Americano().nome)
// console.log(new Americano().__proto__)


Array.prototype.meuForEach = function (callback, thisArgs = null) {
    for (let i = 0; i < this.length; i++) {
        const callbackBindedOnThisArgs = callback.bind(thisArgs || null);
        callbackBindedOnThisArgs(this[i], i, this);
    }
}

Array.prototype.meuMap = function (callback, thisArgs = null) {
    const result = [];

    for (let i = 0; i < this.length; i++) {
        const callbackBindedOnThisArgs = callback.bind(thisArgs || null);
        result.push(callbackBindedOnThisArgs(this[i], i, this));
    }
    return result;
};

Array.prototype.meuReduce = function (callback, initialValue) {
    let accumulator = initialValue !== undefined ? initialValue : this[0];
    let startIndex = initialValue !== undefined ? 0 : 1;

    for (let i = startIndex; i < this.length; i++) {
        accumulator = callback(accumulator, this[i], i, this);
    }
    return accumulator;
};

Array.prototype.meuFind = function (callback, thisArgs = null) {
    for (let i = 0; i < this.length; i++) {
        const callbackBindedOnThisArgs = callback.bind(thisArgs || null);
        if (callbackBindedOnThisArgs(this[i], i, this)) {
            return this[i];
        }
    }
    return undefined;
}

const arr = [1, 2, 3, 4, 5];

const map = arr.meuMap((item) => item * 2);
console.log(map);

const sum = arr.meuReduce((acc, item) => acc + item, 0);
console.log(sum); 

const find = arr.meuFind((item) => item === 3);
console.log(find);
